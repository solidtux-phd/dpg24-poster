#import "@preview/cetz:0.2.1"
#import "../fig.typ": *

#let template(margin: none, it) = it

#if eval(sys.inputs.at("external", default: "false")) {
  import "figtemplate.typ": template as tmp
  template = tmp
}

#show: template

#let width = 30cm
#let line_width = 7pt
#let ny = 7
#let nx = 17
#let hw = 2
#let x0 = 1

#let layer(w: 10, h: 10, pos: 0, color: red, line_width: 2pt) = {
  import cetz.draw: *

  group({
    set-style(stroke: (thickness: line_width, paint: color))
    merge-path(fill: color.transparentize(75%), {
      line((0, pos, 0), (w, pos, 0))
      line((), (w, pos, h))
      line((), (0, pos, h))
      line((), (0, pos, 0))
    })
  })
}

#cetz.canvas(
  length: width / 20,
  {
    import cetz.draw: *

    let dis = 1.45
    let w = 10
    let h = 5
    let text_off = 0.4

    layer(w: w, h: h, color: scc)

    line(
      (0, 0.1, 0),
      (0, dis - 0.1, 0),
      stroke: (thickness: line_width, dash: "loosely-dotted", paint: jc),
      name: "l",
    )
    content("l.mid", text(fill: jc, [$J med$]), anchor: "east")

    group({
      rotate(x: 90deg)
      let mbsr = 0.3
      for y in (0 + mbsr, h - mbsr) {
        circle((w / 2, y), radius: mbsr, stroke: none, fill: kitcolors.red)
      }
    })

    content(
      (7, 0, -text_off),
      anchor: "north-west",
      text(fill: scc, [Superconductor]),
    )

    layer(w: w, h: h, pos: dis, color: gc)

    content((7, dis, h + text_off), anchor: "south-west", text(fill: gc, [Magnet]))

    group({
      translate(y: dis)
      rotate(x: 90deg)
      set-style(stroke: (thickness: 2pt, paint: gc), mark: (end: ">"))

      for ix in range(nx) {
        let x = (ix + 0.5) * w / nx
        let xrel = ix - (nx - 1) / 2

        let phi = if xrel == 0 {
          90deg
        } else {
          -2 * calc.atan(calc.sinh(hw / x0) / calc.sinh(xrel / x0)) - 90deg
        };

        for iy in range(ny) {
          let y = (iy + 0.5) * h / ny
          let dx = 0.4 * calc.cos(phi)
          let dy = 0.4 * calc.sin(phi)

          line((x - 0.5 * dx, y - 0.5 * dy), (x + 0.5 * dx, y + 0.5 * dy))
        }
      }
    })

    content(
      (w / 2, 0, -text_off),
      anchor: "north",
      text(fill: kitcolors.red, [MBS]),
    )

    line(
      (w + 1, 0, h / 2 - 1),
      (w + 1, 0, h / 2 + 1),
      stroke: (thickness: line_width, paint: bc),
      mark: (end: ">"),
      name: "lb",
    )
    content("lb.mid", anchor: "north-west", text(fill: bc, [$thin B$]))
  },
)