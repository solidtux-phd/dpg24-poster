#let template(margin: 0pt, it) = {
  set page(width: auto, height: auto, margin: margin)
  show math.equation: set text(font: "Fira Math")
  set text(
    font: "Fira Sans",
    size: 40pt,
    top-edge: "cap-height",
    bottom-edge: "descender",
  )
  show math.equation: set text(font: "Fira Math")

  align(center, it)
}