#import "@preview/cetz:0.2.1"
#import "../fig.typ": *

#let template(margin: none, it) = it

#if eval(sys.inputs.at("external", default: "false")) {
  import "figtemplate.typ": template as tmp
  template = tmp
}

#show: template.with(margin: (left: 5mm, right: 1mm, top: 1mm, bottom: 4mm))

#let line_width = 5pt

#table(
  align: (right, left),
  stroke: none,
  inset: 0mm,
  columns: (1fr, 1fr),
  column-gutter: field,
  row-gutter: 0.5 * field,
  plot_wind(
    file: "data/0.5_0.5_winding.dat",
    width: 80,
    height: 80,
    title: $B=J M_0=0.5$,
    line_width: line_width,
    color: kitcolors.purple,
    step: eval(sys.inputs.at("step", default: "none")),
  ),
  plot_wind(
    file: "data/2.5_2.5_winding.dat",
    width: 80,
    height: 80,
    title: $B=J M_0=2.5$,
    line_width: line_width,
    color: kitcolors.cyan,
    step: eval(sys.inputs.at("step", default: "none")),
  ),
)

#v(-0.5 * field)

#plot_angle(
  width: 230,
  height: 80,
  line_width: line_width,
  colors: (kitcolors.purple, kitcolors.cyan),
  files: ("data/0.5_0.5_angle.dat", "data/2.5_2.5_angle.dat"),
  labels: ($B=J M_0=0.5$, $B=J M_0=2.5$),
  step: eval(sys.inputs.at("step", default: "none")),
) 