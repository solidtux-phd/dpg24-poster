MEMORY:=20G
RUNCMD:= time systemd-run --scope -p MemoryMax="${MEMORY}" --user
FLAGS:=--font-path ./fonts/ --root .
STEP:=
EXTERNAL:=

ifdef EXTERNAL
	main_deps=$(patsubst %.typ,%.svg,$(wildcard fig/*.typ))
	inputs="external=true"
else
	main_deps=$(wildcard fig/*.typ)
	inputs="external=false"
endif

.PHONY: clean

main.pdf: $(wildcard *.typ) $(main_deps)
	$(RUNCMD) typst compile $(FLAGS) --input $(inputs) --input step=$(STEP) main.typ

fig/%.svg: fig/%.typ fig.typ
	$(RUNCMD) typst compile $(FLAGS) --input $(inputs) --input step=$(STEP) -f svg $<

clean:
	-rm main.pdf fig/*.svg