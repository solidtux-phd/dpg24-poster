# Poster Example

The following parameters exist for the build process via variables

* `EXTERNAL`: if set, use separate files for figures
* `STEP`: if set to a number, only use every n-th point for the plot
* `MEMORY`: limit of available memory (used in the default `RUNNER`)
* `RUNNER`: prefix for the typst command (defaults to a timed systemd-runner that limits the memory usage)

For example,

```sh
make
```

builds the program without externalizing and using every point, while

```sh
make EXTERNAL=true STEP=50
```

externalizes the figures and uses every 50th point. Use `make clean` to remove temporary files and build from scratch.