#import "kitcolors.typ"

#let field = 20mm
#let to_field(x) = calc.round(x / field) * field
#let grid_state = state("grid", false)

#let small(font-size: 34pt, body) = {
  text(size: font-size, body)
}

#let poster_block(title: none, body) = layout(
  size => style(
    styles => locate(
      loc =>{
        let size = measure(block(width: size.width, inset: 0pt, spacing: 0pt, body), styles)
        let num = calc.ceil(size.height / field)
        if title == none {
          block(
            width: 100%,
            breakable: false,
            height: num * field,
            inset: (top: 0.5 * field, bottom: 0.5 * field),
            fill: if grid_state.at(loc) { color.rgb(0, 0, 0, 10%) },
            body,
          )
        } else {
          block(
            width: 100%,
            breakable: false,
            height: (num + 2) * field,
            inset: (top: 0 * field, bottom: 0.5 * field),
            fill: if grid_state.at(loc) { color.rgb(0, 0, 0, 10%) },
            {
              heading(title)
              v(0.5 * field)
              body
            },
          )
        }
      },
    ),
  ),
)

#let poster(
  title: "Title",
  subtitle: "Subtitle",
  author: "Author",
  institute: [],
  lang: "en",
  font: "Fira Sans",
  math-font: "Fira Math",
  grid: false,
  text-size: 50pt,
  header-offset: 11,
  cut-border: false,
  content-border: true,
  before: [],
  after: [],
  body,
) = {
  let motto = (
    "en": "KIT – The Research University in the Helmholtz Association",
    "de": "KIT – Die Forschungsuniversität in der Helmholtz-Gemeinschaft",
  ).at(lang)

  let size = (width: 841mm, height: 1189mm)
  let top_margin = (header-offset + 6) * field
  let bottom_margin = 3.5 * field
  let left_margin = 2 * field
  let right_margin = 2 * field

  set text(font: font, top-edge: 1em, size: text-size)
  set par(leading: 0.4em)
  show par: set block(spacing: 0pt)

  set page(
    paper: "a0",
    background: place(
      style(
        styles => {
          set text(top-edge: field)
          set par(leading: 0pt)
          if grid {
            for iy in range(60) {
              let stroke = 4pt + gray
              if calc.rem(iy, 5) == 0 and iy > 0 {
                stroke = 8pt + gray
                place(
                  bottom,
                  dy: iy * field - 8pt,
                  text(weight: "bold", size: 30pt, fill: gray, repr(iy)),
                )
              }
              place(line(start: (0%, iy * field), end: (100%, iy * field), stroke: stroke))
            }
            place(line(
              start: (0%, size.height - field),
              end: (100%, size.height - field),
              stroke: (thickness: 4pt, paint: gray, dash: "dotted"),
            ))
            for ix in range(42) {
              let stroke = 4pt + gray
              if calc.rem(ix, 5) == 0 and ix > 0 {
                stroke = 6pt + gray
                place(
                  right,
                  dy: 30pt,
                  dx: ix * field - 8pt,
                  text(weight: "bold", size: 30pt, fill: gray, repr(ix)),
                )
              }
              place(line(start: (ix * field, 0%), end: (ix * field, 100%), stroke: stroke))
            }
            place(dy: top_margin, dx: field, rect(
              stroke: 0.5pt + kitcolors.black70,
              width: size.width - 2 * field,
              height: size.height - bottom_margin - top_margin + field,
              fill: color.rgb(0, 0, 0, 10%),
              radius: (bottom-left: field),
            ))
          }
          if cut-border {
            place(rect(width: size.width, height: size.height, stroke: 2mm + black))
          }
          if content-border {
            place(dx: field, dy: field, rect(
              width: size.width - 2 * field,
              height: size.height - bottom_margin,
              stroke: 5pt + kitcolors.black70,
              radius: (bottom-left: field, top-right: field),
            ))
          }
          place(
            dx: 2 * field,
            dy: 2 * field,
            rect(width: 10 * field, height: 4.5 * field, fill: kitcolors.green),
          )
          place(
            right,
            dx: size.width - 2 * field,
            dy: 2 * field,
            align(right, institute),
          )
          place(
            dx: 2 * field,
            dy: header-offset * field,
            block(width: size.width - 4 * field, {
              text(weight: "bold", size: 80pt, title)
              linebreak()
              linebreak()
              text(size: 50pt, fill: kitcolors.black70, subtitle)
              v(0.5 * field)
              text(size: 50pt, author)
            }),
          )
          place(
            left + bottom,
            dx: field,
            dy: size.height - field,
            text(size: 32pt, motto),
          )
          place(
            right + bottom,
            dx: size.width - field,
            dy: size.height - field,
            text(weight: "bold", size: 60pt, "www.kit.edu"),
          )
        },
      ),
    ),
    margin: (
      top: top_margin,
      bottom: bottom_margin,
      left: left_margin,
      right: right_margin,
    ),
  )

  show heading: it => {
    block(
      fill: kitcolors.green,
      height: 1.5 * field,
      width: 100%,
      spacing: 0mm,
      outset: (left: 0.5 * field, right: 0.5 * field),
      radius: (top-right: field),
      above: 1 * field,
      below: 0pt,
      align(
        left + horizon,
        text(fill: white, size: 50pt, top-edge: "cap-height", it),
      ),
    )
  }

  show math.equation: set text(font: math-font)
  show math.equation.where(block: true): it => style(styles => {
    let s = measure(it, styles)
    block(
      height: to_field(s.height),
      width: 100%,
      fill: if grid { color.rgb(255, 255, 0, 30%) },
      spacing: 0pt,
      align(horizon, it),
    )
  })

  show: it => align(center, it)

  grid_state.update(_ => grid)
  layout(size => {
    style(styles => {
      let before_block = block(width: size.width, spacing: 0pt, before)
      let after_block = block(width: size.width, spacing: 0pt, after)
      let before_size = measure(before_block, styles).height
      let after_size = measure(after_block, styles).height - field
      before_block
      block(
        height: 100% - (before_size + after_size),
        width: size.width,
        spacing: 0pt,
        columns(gutter: 2 * field, body),
      )
      place(after_block)
    })
  })
}