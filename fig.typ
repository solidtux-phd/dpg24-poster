#import "@preview/cetz:0.2.1"
#import "kitcolors.typ"
#import "template.typ": field

#let mc(col, body) = text(fill: col, body)
#let scc = kitcolors.brown
#let jc = kitcolors.blue
#let gc = kitcolors.green
#let bc = kitcolors.mai
#let cc = kitcolors.orange

#let mag_plot(
  h: 1,
  x0: 0.2,
  num_points: 100,
  xmax: 3,
  m0: 1,
  b: 0.1,
  line_width: 8pt,
  circle_size: 5mm,
  width: 20,
  height: 20,
) = {
  let my = ()
  for i in range(num_points) {
    let x = 2 * xmax * (i / (num_points - 1) - 0.5)
    let theta = 2 * calc.atan(calc.sinh(h / x0) / calc.sinh(x / x0)) - 180deg
    my.push((x, b + m0 * calc.cos(theta)))
  }

  cetz.canvas(
    length: 1mm,
    {
      import cetz.draw
      import cetz.plot
      draw.set-style(stroke: line_width)
      plot.plot(
        axis-style: "school-book",
        plot-style: (stroke: line_width + gc),
        size: (width, height),
        x-tick-step: none,
        y-tick-step: none,
        x-label: $x$,
        y-label: $#mc(bc, $B$)-#mc(jc, $J$) #mc(gc, $M_y$)$,
        x-min: -xmax,
        x-max: xmax + 0.2,
        y-min: calc.min(-m0 + b, -1) - 0.2,
        y-max: calc.max(m0 + b, 1) + 0.4,
        x-ticks: (),
        y-ticks: (),
        {
          plot.add(my)
          for y in (-1, 1) {
            plot.add-hline(
              y,
              style: (stroke: (thickness: line_width, dash: "loosely-dotted", paint: cc)),
            )
          }
          // plot.annotate(
          //   {
          //     for x in (-xmax, xmax) {
          //       draw.content((x, b - m0), circle(radius: circle_size, fill: kitcolors.blue))
          //     }
          //     draw.content((0, b + m0), circle(radius: circle_size, fill: kitcolors.red))
          //   },
          // )
        },
      )
    },
  )
}

#let plot_wind(
  file: "",
  width: 20,
  height: 20,
  line_width: 8pt,
  title: [],
  color: kitcolors.green,
  step: none,
) = {
  let data = csv(file, delimiter: "\t").map(row => (eval(row.at(0)), eval(row.at(1))))
  if step != none {
    data = range(data.len(), step: step).map(i => data.at(i))
  }

  repr(step)

  cetz.canvas(length: 1mm, {
    import cetz.plot
    import cetz.draw

    draw.set-style(stroke: line_width, axes: (
      stroke: line_width,
      left: (label: (offset: -0.9cm)),
      bottom: (label: (offset: -0.5cm)),
    ))
    plot.plot(
      // axis-style: "school-book",
      // size: (width / 1mm, height / 1mm),
      name: "plot",
      size: (width, height),
      x-min: -1.1,
      x-max: 1.1,
      y-min: -1.1,
      y-max: 1.1,
      x-label: $hat(d)_x$,
      y-label: $hat(d)_z$,
      x-tick-step: none,
      y-tick-step: none,
      y2-label: title,
      plot-style: (stroke: line_width + color),
      x-ticks: (-1, 1),
      y-ticks: (-1, 1),
      // x-equal: "y",
      {
        plot.add(data)
        plot.add-anchor("top", (0, 1.5))
      },
    )
    draw.content("plot.top", title)
  })
}

#let plot_angle(
  width: 20,
  height: 20,
  line_width: 8pt,
  colors: (),
  files: (),
  labels: (),
  step: none,
)= {
  let data = files.map(
    file => {
      let tmp = csv(file, delimiter: "\t").map(row => (eval(row.at(0)), eval(row.at(1)) - 0.5 * calc.pi))
      if step != none {
        tmp = range(tmp.len(), step: step).map(i => tmp.at(i))
      }
      tmp
    },
  )

  cetz.canvas(
    length: 1mm,
    {
      import cetz.plot
      import cetz.draw

      draw.set-style(
        stroke: line_width,
        axes: (stroke: line_width, left: (label: (offset: 10))),
      )
      plot.plot(
        size: (width, height),
        x-label: $k_y$,
        y-label: $op("atan")(d_z, d_x) - pi/2$,
        x-tick-step: 2,
        x-min: -7,
        x-max: 7,
        y-tick-step: none,
        y-min: -1.5 * calc.pi,
        y-max: 3.5 * calc.pi,
        y-ticks: (0, (2 * calc.pi, [$2pi$])),
        legend: "legend.inner-north-west",
        legend-style: (
          padding: 5,
          stroke: none,
          fill: none,
          item: (preview: (width: 10, margin: 1)),
        ),
        {
          for (i, d) in data.enumerate() {
            plot.add(style: (stroke: line_width + colors.at(i)), label: labels.at(i), d)
          }
        },
      )
    },
  )
}

#let mbs_plot(width: 10 * field) = {
  let img = image("plots/ldos-hb-3a.png", width: width - field)
  let size = measure(img)
  table(
    columns: (field, width - field),
    rows: (auto, size.height, auto),
    align: center + horizon,
    gutter: 0pt,
    stroke: none,
    inset: 3pt,
    [],
    [$abs(Psi)^2$],
    align(right + top)[
      $-5$
      #v(1fr)
      $x$
      #v(1fr)
      $5$
    ],
    img,
    [],
    align(top, [
      #v(-0.5cm)
      $-60$
      #h(1fr)
      $y$
      #h(1fr)
      $60$
    ]),
  )
}
