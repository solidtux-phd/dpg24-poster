#import "template.typ": *
#import "kitcolors.typ"
#import "fig.typ": *

// Show debug grid
#let grid = false

#let line_width = 5pt
#let im = $upright(i)$

#show: poster.with(
  title: [2#sym.pi Domain Walls for Tunable Majorana Devices],
  subtitle: [Finding and Tuning Majoranas in Superconductor-Magnet-Hybrids],
  author: [
    _Daniel Hauck_, Stefan Rex, Markus Garst
  ],
  institute: [Institute of Theoretical Solid State Physics],
  header-offset: 8,
  text-size: 40pt,
  cut-border: true,
  content-border: false,
  grid: grid,
  after: align(
    left,
    {
      block(
        height: 5 * field,
        {
          heading[References]
          v(0.25 * field)
          set text(size: 30pt)
          show bibliography: set block(spacing: 10pt)
          columns(2, bibliography("literature.bib", style: "style.csl", title: none))
        },
      )
    },
  ),
)

#poster_block(
  title: [Magnet Superconductor Hybrids],
  layout(
    size => {
      align(
        left,
      )[
        #v(0pt, weak: true)
        - two-dimensional layered system
        - Zeeman coupling between layers
        - Majorana bound states (MBS) predicted for various systems @Rex:2020utd
          @Gungordu:2017atn @Yang:2016ltv @Kim:2015
      ]
      v(-field)
      if eval(sys.inputs.at("external", default: "false")) {
        image("fig/sketch.svg")
      } else {
        include "fig/sketch.typ"
      }
      v(-0.5 * field)
    },
  ),
)

#poster_block(
  title: [Why 2#sym.pi Domain Walls?],
  align(
    left,
  )[
    #v(0pt, weak: true)
    - easily realizable in magnetic layered structures
    - manipulable by spin currents @Ryu:2013 @Emori:2013 or fields @Hu:2016
      @Togawa:2012\
    - proposed previously as a promising platform @Kim:2015
    - in-plane only fields possible
  ],
)

#v(-0.25 * field)

#poster_block(
  title: "Model",
  {
    $
      H =&
      #mc(scc, $(k^2/(2m)-mu)tau_z$)
      + #mc(scc, $Delta tau_x$)
      + #mc(scc, $alpha (arrow(k) times arrow(sigma))_z tau_z$)
      - #mc(jc, $J$)
      #mc(gc, $arrow(M) dot arrow(sigma)$)
      + #mc(bc, $B sigma_y$)
    $

    $
      #mc(gc, $arrow(M)(arrow(r))$) = M_0 vec(sin(theta)\ cos(theta)\ 0)
      #h(1em)
      theta = 2 op("sgn")(x)op("atan")(sinh(h "/" x_0)/sinh(x "/" x_0))
    $

    small([
      $m$: mass,
      $mu$:~chemical~potential,
      $Delta$:~s\u{2011}wave~gap,
      $alpha$:~spin\u{2011}orbit~coupling,
      $J$:~interlayer~coupling,
      $arrow(M)$:~magnetization,
      $B$:~magnetic~field
    ])

    v(-0.75 * field)

    table(
      stroke: none,
      inset: 0mm,
      columns: (1fr, 1fr),
      rows: 4 * field,
      align: (right + horizon, left + horizon),
      column-gutter: field,
      mag_plot(b: 0.6, width: 130, height: 65, line_width: line_width),
      small(
        [
          $M_0$:~magnetization~amplitude,
          $h$:~domain~wall~width,
          $x_0$:~domain~wall~steepness,
          #box(width: 1em, height: 1em, baseline: 0em, fill: cc, stroke: none)~$B_upright(c)=sqrt(Delta^2+mu^2)$
        ],
      ),
    )

    v(-1 * field)
  },
)

#v(0.25 * field)

#poster_block(title: [Numerical Result], context{
  table(
    align: (center + horizon, left + horizon),
    columns: (2fr, 1fr),
    rows: 3 * field,
    stroke: none,
    gutter: 0pt,
    {
      v(-0.5cm)
      mbs_plot(width: 9 * field)
    },
    inset: 0pt,
    small[
      #box($alpha=0.2$),
      #box($B=-J M_0=2.5$),
      #box($h=0.4$),
      #box($x_0=0.04$),
      #box($Delta=1$),
      #box($mu=0$),
      #box($m=1$),
    ],
  )
})

#poster_block(
  title: "Topology",
  {
    $
      H_"1d" = &
      #mc(scc, $((k_y^2-diff_x^2)/(2m) - mu) tau_z$)
      + #mc(scc, $Delta tau_x$)
      + #mc(scc, $alpha (im diff_x sigma_y - k_y sigma_x) tau_z$)\
               &- #mc(jc, $J$)
      #mc(gc, $arrow(M) dot arrow(sigma)$)
      + #mc(bc, $B sigma_y$)
      \
      H_"ref" =&
      #mc(scc, $(k_y^2/(2m) - mu) tau_z$)
      + #mc(scc, $Delta tau_x$)
      - #mc(jc, $J$)
      + #mc(bc, $B sigma_y$)
      = lr(size: #150%, H_"1d"|)_(alpha=0,x=0)
    $
    $
      arrow(d)_(i,n)(k_y) &prop limits(integral)_(-infinity)^infinity
      #h(-0.25em)
      upright(d)x
      thin
      arrow(phi)_n^dagger (x,k_y)
      rho_i
      arrow(phi)_n (x,k_y)
      \
      rho_i               &= U^dagger sigma_i U
      quad
      U H_"ref" U^dagger
      = mat(epsilon_+, 0;0, epsilon_-)
      \
      epsilon_plus.minus  &=
      inline(plus.minus sqrt(Delta^2+(k_y^2/(2m) - mu)^2) minus.plus B)
    $

    v(0.5 * field)
    small[
      $arrow(phi)_n$:~$n$th~eigenvector~of~$H_"1d"$,
      $sigma$:~Pauli~matrix,
      $rho$:~rotated~Pauli~matrix,
      $U in CC^(2 times 4)$:~transformation~to~low-energy~space~of~trivial~$H_"ref"$
    ]

    if eval(sys.inputs.at("external", default: "false")) {
      image("fig/winding.svg")
    } else {
      include "fig/winding.typ"
    }
  },
)

#v(-0.30 * field)

#poster_block(title: [Summary & Outlook], align(left)[
  #v(0pt, weak: true)
  - MBS can be found using magnetic domain walls
  - topology with in-plane fields still possible
  - investigate dynamical features\
    #sym.arrow.r.double braiding?
])